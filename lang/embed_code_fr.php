<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/embed_code.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_label_hauteur' => 'Hauteur par défaut',
	'cfg_label_largeur' => 'Largeur par défaut',
	'cfg_titre' => 'Configuration du plugin Embed code',

	// I
	'info_embed_code' => 'Intégrer ce média',

	// L
	'label_code_embed' => 'Code à copier',
	'label_hauteur' => 'Hauteur',
	'label_largeur' => 'Largeur',
	'lien_preview_embed' => 'Prévisualiser le rendu',

	// M
	'message_document_inexistant' => 'Ce document n’est pas disponible sur le site <a href="@url_site@">@nom_site@</a>.'
);
