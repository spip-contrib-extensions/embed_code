<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-embed_code?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'embed_code_description' => 'Maak code aan voor het opnemen van documenten in andere sites.',
	'embed_code_nom' => 'Embed code',
	'embed_code_slogan' => 'Aanmaken van ingebedde code voor documenten'
);
