<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/embed_code.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'embed_code_description' => 'Génèrer des codes d’incrustation distants de documents pour utilisation sur d’autres sites.',
	'embed_code_nom' => 'Embed code',
	'embed_code_slogan' => 'Génération de code d’incrustation pour documents'
);
